package com.company;

import java.util.Scanner;

public class homeworkPartC1 {

    public static void main(String[] args) {

        //1. Ввести n строк с консоли, найти самую короткую строку. Вывести эту строку и ее длину.

        Scanner newSc = new Scanner(System.in); //Вызываем масив с названием newSc

        System.out.println("Enter number of rows: ");
        int numRows = Integer.parseInt(newSc.nextLine());   //Считали количество строк с консоли, которые ввел user с помощью nextLine()
        String[] rowsArray = new String[numRows];   //Определились с колиством индексов в массиве, с помощью int numRows

        for (int i = 0; i < numRows; i++) {
            System.out.println(String.format("Enter the row number %d: ", i + 1)); //консоль говорит нам введи что-то начиная с первой строки
            String row = newSc.nextLine();   //сканер считывает то что ты вбил в консоли
            rowsArray[i] = row;     //теперь массиву rowsArray присваиваеться то, что мы написали выше
        }

        //Здесь мы должны начинать вычислять самую короткую строку
        int minLength = rowsArray[0].length();  //начиная с массива с индексом 0, мы можем начать вычислять самую короткую строку
        String minLengthRow = rowsArray[0];


        for (int i = 0; i < numRows; i++) {
            String currentRowByIndex = rowsArray[i];        //текущая строка которая взял по индексу из массива rowsArray
            if (minLength > currentRowByIndex.length()) {       //сравниваю минимальное значение с размером текущей строки
                minLength = currentRowByIndex.length();     //сохраняю длину текущей стркои по индексу
                minLengthRow = currentRowByIndex;   //СОхраняю строку для того чтобы ее вывести ниже
            }
        }

        //print row with max length
        System.out.println("The lowest row is: " + minLengthRow);


    }
}
